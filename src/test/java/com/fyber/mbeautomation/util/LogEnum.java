package com.fyber.mbeautomation.util;

/**
 * Created by mofazzal on 6/4/15.
 */
public enum LogEnum {
    SDK_STARTED("SDK Started"), VIDEO_REQUEST("Video Requested"), VIDEO_AVAILABLE("Video:Available"), REWARD_MESSAGE("Reward Received");

    private String name;

    private LogEnum(String lName){
        name = lName;
    }

    public String getLogEnum(){
        return name;
    }
}
