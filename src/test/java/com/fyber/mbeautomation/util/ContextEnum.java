package com.fyber.mbeautomation.util;

/**
 * Created by mofazzal on 6/3/15.
 */
public enum ContextEnum {
    NATIVE("NATIVE"), WEBVIEW("WEBVIEW");

    private String contextName;

    private ContextEnum(String cName){
        contextName = cName;
    }

    public String getContextName(){
        return contextName;
    }
}
