package com.fyber.mbeautomation.util;

import io.appium.java_client.AppiumDriver;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.sikuli.script.Finder;
import org.sikuli.script.Pattern;
import org.sikuli.script.Region;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;

/**
 * Created by mofazzal on 6/3/15.
 */
public class Util {

    private static AppiumDriver driver;
    private static WebDriverWait driverWait;
    private static String matchDirecotry;

    public static void init(AppiumDriver webDriver) {
        driver = webDriver;
        int timeout = 10;

        driverWait = new WebDriverWait(webDriver, timeout);
    }

    public static boolean isElementPresent(String elementXpath) throws Exception{

        int counter = 0;
        try{
//            boolean elementNotPresent = true;
//            while (elementNotPresent){
                driverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(elementXpath)));
//                elementNotPresent = !(driver.findElement(By.xpath(elementXpath)).isDisplayed());
//            }
        }catch (WebDriverException ex)
        {
            if (ex.getMessage().toLowerCase().contains("An element could not be located") || counter != 10)
            {
                isElementPresent(elementXpath);
                counter++;
            }else
            {
                return false;
            }
        }
        return true;
    }


    public static boolean captureScreenShot(String fileName) throws IOException {


        String userDir = System.getProperty("user.dir");
        DateFormat df = new SimpleDateFormat("ddMMyyyy");
        Date today = Calendar.getInstance().getTime();
        String date = df.format(today);

        String dirName = "screenshots_" + date;

        File dir = new File(userDir + "/" + dirName);
        matchDirecotry = userDir + "/" + dirName + "/";

        if (!dir.exists()) {
            try {
                dir.mkdir();
            } catch (SecurityException ex) {
                System.out.println("Directory creation failed");
            }
        }

        try {
            WebDriver augmentedDriver = new Augmenter().augment(driver);
            File f = ((TakesScreenshot) augmentedDriver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(f, new File(userDir + "/" + dirName + "/" + fileName));
            return true;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return false;

    }

    public static boolean compareScreenShot(String fileName) throws IOException {

        String baseDir = System.getProperty("user.dir") + "/base_screenshots/";

        Image image = ImageIO.read(new File(matchDirecotry + fileName));
        System.out.println(matchDirecotry + fileName);
        Finder finder = new Finder(matchDirecotry + fileName, new Region(0, 0, image.getHeight(null), image.getWidth(null)));
        Pattern pattern = new Pattern(baseDir + fileName);
        finder.find(pattern);

        if (finder.hasNext()) {
            return true;
        }

        return false;
    }

    public static void setContextIfAvailable(ContextEnum contextEnum){

        Set<String> contextNames = driver.getContextHandles();

        if(isContextAvailable(contextEnum.getContextName(), contextNames)){
            int index = getContextIndex(contextEnum.getContextName(), contextNames);
            System.out.println("Inside context available "+index);
            driver.context((String)contextNames.toArray()[index]);
            System.out.println("context set "+contextEnum.getContextName());
        }else {
            System.out.println("context not available "+contextEnum.getContextName());
        }

    }

    private static boolean isContextAvailable(String contextName, Set<String> contextSet){
        for(String context: contextSet){
            System.out.println(context);
            if (context.startsWith(contextName))
                return true;
        }
        return false;
    }

    private static int getContextIndex(String contextName, Set<String> contextSet){
        int index = 0;
        for(String context: contextSet){
            if (context.startsWith(contextName)){
                return index;
            }
            index++;
        }
        return 0;
    }

    public static boolean findTextInAppLog(LogEnum logEnum, String xPath)
    {
        String needle = logEnum.getLogEnum();
        String haystack = driver.findElement(By.xpath(xPath)).getText();
        System.out.println(haystack);

        return haystack.toLowerCase().contains(needle.toLowerCase());

    }

    public static void clearAppConsole()
    {
        driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAButton[6]")).click();
    }


}
