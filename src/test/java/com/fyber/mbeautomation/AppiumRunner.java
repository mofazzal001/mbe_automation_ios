package com.fyber.mbeautomation;

import com.fyber.mbeautomation.util.ContextEnum;
import com.fyber.mbeautomation.util.Util;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.URL;
import java.nio.file.Paths;
import java.util.Date;


/**
 * Created by mofazzal on 6/3/15.
 */

public class AppiumRunner {

    public static AppiumDriver driver;
    private static final Date date = new Date();

    @BeforeClass
    public static void setUp() throws Exception
    {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("appium-version", "1.3.7");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "ios");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "8.3");
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "iPhone 6");
        capabilities.setCapability("noReset", true);
        capabilities.setCapability("fullReset", false);

        capabilities.setCapability("name", "MBE Automation"+date);

        String userDir = System.getProperty("user.dir");
        String localApp = "app/RewardVideoTest.app";
        String appPath = Paths.get(userDir, localApp).toAbsolutePath().toString();
        capabilities.setCapability("app", appPath);
        driver = new IOSDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);

        Util.init(driver);

        Util.setContextIfAvailable(ContextEnum.NATIVE);

    }

    /**
     * Run after each class *
     */
    @AfterClass
    public static void tearDown() throws Exception {
        //if (driver != null) driver.quit();
    }


}
