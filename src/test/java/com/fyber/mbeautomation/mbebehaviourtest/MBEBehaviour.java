package com.fyber.mbeautomation.mbebehaviourtest;

import com.fyber.mbeautomation.AppiumRunner;
import com.fyber.mbeautomation.util.ContextEnum;
import com.fyber.mbeautomation.util.LogEnum;
import com.fyber.mbeautomation.util.Util;
import junit.framework.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by mofazzal on 6/3/15.
 */
//@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MBEBehaviour extends AppiumRunner{

    private boolean isSDKStarted = false;
    private boolean isVideoRequested = false;
    private boolean isVideoAvailable = false;
    private boolean isVideoPlayed = false;

    private boolean startSDK() throws Exception
    {
        if(Util.isElementPresent("//UIAApplication[1]/UIAWindow[1]/UIAButton[1]"))
        {
            driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAButton[1]")).click();
            return true;
        }
        return false;
    }

    @Test
    public void testStartSDK() throws Exception
    {

        //check that at the console of the app Started SDK is text is present
        assertTrue("Start SDK function failed", this.startSDK());
        isSDKStarted = true;
        assertTrue("SDK started failed", Util.findTextInAppLog(LogEnum.SDK_STARTED, "//UIAApplication[1]/UIAWindow[1]/UIATextView[1]"));
        //capture a screen shot for reference
        assertTrue(Util.captureScreenShot("sdk_started.png"));
    }

    private boolean requestVideo() throws Exception
    {
        while(!isSDKStarted)
        {
            isSDKStarted = this.startSDK();
        }
        if(Util.isElementPresent("//UIAApplication[1]/UIAWindow[1]/UIAButton[2]"))
        {
            driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAButton[2]")).click();
            return true;
        }
        return false;
    }

    @Test
    public void testRequestVideo() throws Exception
    {
        //clearing the app console
        Util.clearAppConsole();

        assertTrue("Request video function failed", this.requestVideo());
        isVideoRequested = true;
        //check that at the console of the app video request text is present
        assertTrue(Util.findTextInAppLog(LogEnum.VIDEO_REQUEST, "//UIAApplication[1]/UIAWindow[1]/UIATextView[1]"));
        //capture a screen shot for reference
        assertTrue(Util.captureScreenShot("request_initiated.png"));

    }

    private boolean checkVideoIsAvailable() throws Exception
    {
        while (!isVideoRequested)
        {
            isVideoRequested = this.requestVideo();
        }
        Thread.sleep(5000);
        if(Util.isElementPresent("//UIAApplication[1]/UIAWindow[1]/UIAButton[3]"))
        {
            driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAButton[3]")).click();
            return true;
        }
        return false;
    }


    @Test
    public void testCheckVideoIsAvailable() throws Exception
    {

        Util.clearAppConsole();

        assertTrue("Check video function failed", this.checkVideoIsAvailable());
        isVideoAvailable = true;
        //check that at the console of the app video is availble text is present
        assertTrue("Video not available", Util.findTextInAppLog(LogEnum.VIDEO_AVAILABLE, "//UIAApplication[1]/UIAWindow[1]/UIATextView[1]"));
        //capture a screen shot for reference
        assertTrue("Image captured failed",Util.captureScreenShot("video_available.png"));

    }

    private boolean loadVideo() throws Exception
    {
        while(!isVideoRequested)
        {
            isVideoRequested = this.requestVideo();
        }

        while (!isVideoAvailable)
        {
            isVideoAvailable = Util.findTextInAppLog(LogEnum.VIDEO_AVAILABLE, "//UIAApplication[1]/UIAWindow[1]/UIATextView[1]");
        }

        if(Util.isElementPresent("//UIAApplication[1]/UIAWindow[1]/UIAButton[4]"))
        {
            driver.findElement(By.xpath("//UIAApplication[1]/UIAWindow[1]/UIAButton[4]")).click();
            return true;
        }

        return false;
    }

    @Test
    public void testLoadVideo() throws Exception
    {
        Util.clearAppConsole();

        assertTrue("Load video function failed", this.loadVideo());
        isVideoPlayed = true;

        //todo: need to change other waiting options
        Thread.sleep(5000);

        //checking whether video element is presented and then take a screenshot
        if(Util.isElementPresent("//UIAApplication[1]/UIAWindow[1]/UIAElement[1]"))
        {
            //todo: need to change other waiting options
            Thread.sleep(5000);
            assertTrue(Util.captureScreenShot("video_loaded.png"));
        }
        assertTrue("Close Video failed", this.closeVideo());

    }

    private boolean closeVideo() throws Exception
    {
        boolean isFinalImageShown = false;
        while (!isFinalImageShown)
        {
            isFinalImageShown = Util.isElementPresent("//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]");
            Thread.sleep(2000);
        }

        Util.setContextIfAvailable(ContextEnum.WEBVIEW);
        driver.executeScript("MBE.finish();");
        Thread.sleep(2000);

        Util.setContextIfAvailable(ContextEnum.NATIVE);
        if(Util.isElementPresent("//UIAApplication[1]/UIAWindow[1]/UIAButton[4]"))
        {
            return true;
        }

        return false;
    }

    private boolean checkVideoReachedEnd() throws Exception
    {
        while (!isVideoPlayed)
        {
            isVideoPlayed = this.loadVideo();
        }
        Thread.sleep(10000);
        if(Util.isElementPresent("//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]"))
        {
            return true;
        }
        return false;
    }

    @Test
    public void testCheckVideoReachedEnd() throws Exception
    {

        assertTrue("Check Video reached end failed", this.checkVideoReachedEnd());

        assertTrue("Close Video failed", this.closeVideo());

        Thread.sleep(5000);
        assertTrue(Util.findTextInAppLog(LogEnum.REWARD_MESSAGE, "//UIAApplication[1]/UIAWindow[1]/UIATextView[1]"));

    }
}
